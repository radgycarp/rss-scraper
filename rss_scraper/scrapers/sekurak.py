from bs4 import BeautifulSoup, CData
import requests
from rss_scraper.config import HOST, DIR

NAME = 'sekurak.xml'

base_rss = requests.get('https://feeds.feedburner.com/sekurak_full').text
xml_soup = BeautifulSoup(base_rss, features='xml')

xml_soup.channel.link.attrs = {'href': f'http://{HOST}/{NAME}'}

for item in xml_soup.find_all('item'):
    link = item.find('link').text
    doc_content = requests.get(link).text
    doc_soup = BeautifulSoup(doc_content, 'html.parser')
    entry = doc_soup.find('div', class_='entry')
    cdata = CData(str(entry).replace('\n', ''))
    desc = item.find('description')
    desc.clear()
    desc.append(cdata)

with open(f'{DIR}/{NAME}', 'w') as f:
    f.write(str(xml_soup))